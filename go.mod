module bitbucket.org/repustate/named-entity-recognition-apis

go 1.13

require (
	cloud.google.com/go v0.47.0
	github.com/adam-hanna/arrayOperations v0.2.5
	github.com/aws/aws-sdk-go v1.25.17
	github.com/golang/protobuf v1.3.2
	github.com/joho/godotenv v1.3.0
	github.com/urfave/cli v1.22.1
	google.golang.org/genproto v0.0.0-20191009194640-548a555dbc03
)

package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"regexp"
	"sync"

	"bitbucket.org/repustate/named-entity-recognition-apis/pkg/provider"
	"bitbucket.org/repustate/named-entity-recognition-apis/pkg/provider/aws"
	"bitbucket.org/repustate/named-entity-recognition-apis/pkg/provider/aylien"
	"bitbucket.org/repustate/named-entity-recognition-apis/pkg/provider/azure"
	"bitbucket.org/repustate/named-entity-recognition-apis/pkg/provider/dandelion"
	"bitbucket.org/repustate/named-entity-recognition-apis/pkg/provider/gcp"
	"bitbucket.org/repustate/named-entity-recognition-apis/pkg/provider/textrazor"

	"github.com/adam-hanna/arrayOperations"
	"github.com/joho/godotenv"
	"github.com/urfave/cli"
)

const (
	sampleInputDir    = "sample_input/"
	sampleOutputDir   = "sample_output/"
	sampleExpectedDir = "sample_expected/"
	resultsDir        = "results/"
)

var filenameRegexp = regexp.MustCompile(`input(\d+)\-(.*)\.txt`)

func init() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
}

func main() {
	app := cli.NewApp()
	app.Name = "ner-apis"
	app.Version = "0.0.1"
	app.Commands = []cli.Command{
		{
			Name:   "start",
			Usage:  "Start service",
			Action: start,
			Flags: []cli.Flag{
				cli.BoolFlag{Name: "all", Usage: "Enable all analyzers"},
				cli.BoolFlag{Name: "gcp", Usage: "Enable GCP Natural Language analyzer"},
				cli.BoolFlag{Name: "aws", Usage: "Enable AWS Comprehend analyzer"},
				cli.BoolFlag{Name: "azure", Usage: "Enable Azure Text Analytics analyzer"},
				cli.BoolFlag{Name: "dandelion", Usage: "Enable Dandelion analyzer"},
				cli.BoolFlag{Name: "textrazor", Usage: "Enable TextRazor analyzer"},
				cli.BoolFlag{Name: "aylien", Usage: "Enable Aylien analyzer"},
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		fmt.Println(err)
	}
}

func start(c *cli.Context) error {
	files, err := ioutil.ReadDir(sampleInputDir)
	if err != nil {
		return err
	}

	var wg sync.WaitGroup

	if c.Bool("gcp") || c.Bool("all") {
		wg.Add(1)
		go runAnalyzer(gcp.NewAnalyzer(sampleOutputDir), files, &wg)
	}

	if c.Bool("aws") || c.Bool("all") {
		wg.Add(1)
		go runAnalyzer(aws.NewAnalyzer(sampleOutputDir), files, &wg)
	}

	if c.Bool("azure") || c.Bool("all") {
		wg.Add(1)
		// Azure has rate limit 20 requests per a minute
		// So this separate function accumulates all inputs in a single batch request
		go runAzureAnalyzer(azure.NewAnalyzer(sampleOutputDir), files, &wg)
	}

	if c.Bool("dandelion") || c.Bool("all") {
		wg.Add(1)
		go runAnalyzer(dandelion.NewAnalyzer(sampleOutputDir), files, &wg)
	}

	if c.Bool("textrazor") || c.Bool("all") {
		wg.Add(1)
		go runAnalyzer(textrazor.NewAnalyzer(sampleOutputDir), files, &wg)
	}

	if c.Bool("aylien") || c.Bool("all") {
		wg.Add(1)
		go runAnalyzer(aylien.NewAnalyzer(sampleOutputDir), files, &wg)
	}

	wg.Wait()
	return nil
}

func runAnalyzer(analyzer provider.Provider, files []os.FileInfo, wg *sync.WaitGroup) {
	defer wg.Done()
	var matchings []float64
	var respTimes []float64

	fmt.Printf("[%s] START\n", analyzer.Name())
	for _, f := range files {
		//if f.Name() != "input46-en.txt" {
		//	continue
		//}
		fmt.Printf("[%s] Analyzing file: %s\n", analyzer.Name(), f.Name())
		match := filenameRegexp.FindStringSubmatch(f.Name())
		num, lang := match[1], match[2]
		// read input file
		input, err := ioutil.ReadFile(sampleInputDir + f.Name())
		if err != nil {
			log.Fatal(err)
		}

		// analyze input text
		output, t, err := analyzer.AnalyzeEntities(input, lang)
		respTimes = append(respTimes, t)
		if err != nil {
			log.Fatalf("[%s] Analyze error: %s", analyzer.Name(), err)
		}

		// save output as json
		err = analyzer.SaveOutput(fmt.Sprintf("output-%s-%s.json", num, lang), output)
		if err != nil {
			log.Fatalf("[%s] Save output error: %s", analyzer.Name(), err)
		}

		// get actual entities from the output
		actual, err := analyzer.GetEntities(output)
		if err != nil {
			log.Fatalf("[%s] Get entities error: %s", analyzer.Name(), err)
		}

		// read expected entities
		expectedBytes, err := ioutil.ReadFile(sampleExpectedDir + fmt.Sprintf("expected%s-%s.txt", num, lang))
		if err != nil {
			log.Fatalf("[%s] Read expected entites error: %s", analyzer.Name(), err)
		}

		var expected []string
		err = json.Unmarshal(expectedBytes, &expected)
		if err != nil {
			log.Fatalf("[%s] Unmarshal expected entities error: %s", analyzer.Name(), err)
		}

		// compare expected and actual entities and save the matching result
		m := calculateMatching(actual, expected)
		// disregard zero matching results if there are no actual entities for some reason and response time is zero
		if len(actual) != 0 || t != 0.0 {
			matchings = append(matchings, m)
		}
		// save all matching results for consistency, even zero ones
		err = analyzer.SaveMatch(m, fmt.Sprintf("matching-%s-%s.txt", num, lang))
		if err != nil {
			log.Fatalf("[%s] Save match error: %s", analyzer.Name(), err)
		}
	}

	err := saveAverageResults(
		analyzer,
		[]byte(fmt.Sprintf("%.2f", calculateAverage(matchings))),
		[]byte(fmt.Sprintf("%.2f", calculateAverage(respTimes))),
	)
	if err != nil {
		log.Fatalf("[%s] Save results error: %s", analyzer.Name(), err)
	}
}

func runAzureAnalyzer(analyzer provider.Provider, files []os.FileInfo, wg *sync.WaitGroup) {
	defer wg.Done()
	var matchings []float64

	fmt.Printf("[%s] START\n", analyzer.Name())
	var data []map[string]string
	for _, f := range files {
		match := filenameRegexp.FindStringSubmatch(f.Name())
		input, err := ioutil.ReadFile(sampleInputDir + f.Name())
		if err != nil {
			log.Fatal(err)
		}
		data = append(data, map[string]string{"id": f.Name(), "language": match[2], "text": string(input)})

	}

	documents := map[string]interface{}{
		"documents": data,
	}

	input, err := json.Marshal(documents)
	if err != nil {
		log.Fatalf("[%s] Marshal documents error: %s", analyzer.Name(), err)
	}

	// analyze input text
	out, t, err := analyzer.AnalyzeEntities(input, "")
	if err != nil {
		log.Fatalf("[%s] Analyze error: %s", analyzer.Name(), err)
	}

	var o azure.TextAnalyticsOutput
	err = json.Unmarshal(out, &o)
	if err != nil {
		log.Fatalf("[%s] Unmarshal output error: %s", analyzer.Name(), err)
	}

	var actual []string

	// save output as json per each input
	for _, d := range o.Documents {
		match := filenameRegexp.FindStringSubmatch(d.ID)
		num, lang := match[1], match[2]

		b, err := json.Marshal(d)
		if err != nil {
			log.Fatalf("[%s] Marshal document error: %s", analyzer.Name(), err)
		}

		var outBuf bytes.Buffer
		err = json.Indent(&outBuf, b, "", "  ")
		if err != nil {
			log.Fatal(err)
		}

		err = analyzer.SaveOutput(fmt.Sprintf("output-%s-%s.json", num, lang), outBuf.Bytes())
		if err != nil {
			log.Fatalf("[%s] Save output error: %s", analyzer.Name(), err)
		}

		// get actual entities from the output
		for _, e := range d.Entities {
			actual = append(actual, e.Name)
		}

		// read expected entities
		expectedBytes, err := ioutil.ReadFile(sampleExpectedDir + fmt.Sprintf("expected%s-%s.txt", num, lang))
		if err != nil {
			log.Fatalf("[%s] Read expected entites error: %s", analyzer.Name(), err)
		}

		var expected []string
		err = json.Unmarshal(expectedBytes, &expected)
		if err != nil {
			log.Fatalf("[%s] Unmarshal expected entities error: %s", analyzer.Name(), err)
		}

		// compare expected and actual entities and save the matching result
		m := calculateMatching(actual, expected)
		matchings = append(matchings, m)
		err = analyzer.SaveMatch(calculateMatching(actual, expected), fmt.Sprintf("matching-%s-%s.txt", num, lang))
		if err != nil {
			log.Fatalf("[%s] Save match error: %s", analyzer.Name(), err)
		}
	}

	//save also errors
	for _, e := range o.Errors {
		match := filenameRegexp.FindStringSubmatch(e.ID)
		num, lang := match[1], match[2]

		b, err := json.Marshal(map[string]interface{}{"error": e.Message})
		if err != nil {
			log.Fatal(err)
		}
		analyzer.SaveOutput(fmt.Sprintf("output-%s-%s.json", num, lang), b)
		analyzer.SaveMatch(0.0, fmt.Sprintf("matching-%s-%s.txt", num, lang))
	}

	err = saveAverageResults(
		analyzer,
		[]byte(fmt.Sprintf("%.2f", calculateAverage(matchings))),
		[]byte(fmt.Sprintf("%.2f", t)),
	)
	if err != nil {
		log.Fatalf("[%s] Save results error: %s", analyzer.Name(), err)
	}
}

func calculateMatching(actual, expected []string) float64 {
	i, _ := arrayOperations.Intersect(actual, expected)

	inter := i.Interface().([]string)

	// if intersection is the same length as expected - return 1.0
	if len(expected) == len(inter) {
		return 1.0
	}

	// if expected is zero length - return 0.0
	if len(expected) == 0 {
		return 0.0
	}

	// otherwise calculate percentage
	return float64(len(inter)) / float64(len(expected))
}

func calculateAverage(n []float64) float64 {
	total := 0.0

	for _, v := range n {
		total += v
	}

	return total / float64(len(n))
}

func saveAverageResults(analyzer provider.Provider, matching, response []byte) error {
	if _, err := os.Stat(resultsDir + analyzer.ShortName()); os.IsNotExist(err) {
		os.MkdirAll(resultsDir+analyzer.ShortName(), 0700)
	}

	avgMatchFile, err := os.Create(resultsDir + analyzer.ShortName() + "/" + "average_match.txt")
	if err != nil {
		return err
	}

	defer avgMatchFile.Close()
	_, err = avgMatchFile.Write(matching)
	if err != nil {
		return err
	}

	avgRespFile, err := os.Create(resultsDir + analyzer.ShortName() + "/" + "average_response_time.txt")
	if err != nil {
		return err
	}

	defer avgRespFile.Close()
	_, err = avgRespFile.Write(response)
	return err
}

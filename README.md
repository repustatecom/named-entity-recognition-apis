# NER provider comparison

## Prerequisites

1. Golang version 1.13.1 or later
2. Python
3. Create a `.env` file with the appropriate values based on `.env.example`

## Getting started

1. Build service
    ```
    go build -o ner-apis main.go
    ```

2. Run the service
    ```
    ./ner-apis start --all
    ```

Also, you can run the service for a specific provider. To get the list of available providers run the following:
```
./ner-apis start --help
```

## spaCy

1. Install `spacy` on MacOS using [virtualenv](https://docs.python-guide.org/dev/virtualenvs/):
    ```
        python -m venv .env
        source .env/bin/activate
        pip install -U spacy
    ```

2. Install trained language models:
    ```
    while read model; do python -m spacy download $model; done < spacy_models
    ```

3. Run the script:
    ```
    python ner_spacy.py
    ```

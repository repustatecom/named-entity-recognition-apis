package language

func UnsupportedResponse() []byte {
	return []byte(`{"language": "is not supported"}`)
}

func SizeLimitExceededResponse() []byte {
	return []byte(`{"text size": "is exceeded limit"}`)
}

func ServerError() []byte {
	return []byte(`{"server": "error"}`)
}

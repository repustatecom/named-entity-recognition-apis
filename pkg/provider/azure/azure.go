package azure

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"

	"bitbucket.org/repustate/named-entity-recognition-apis/pkg/provider"
)

const uriPath = "/text/analytics/v2.1/entities"

// Analyzer represents Azure Text Analytics client
type Analyzer struct {
	outputDir string
}

// TextAnalyticsOutput represents the API response
type TextAnalyticsOutput struct {
	Documents []doc `json:"documents"`
	Errors    []errorResp
}

type errorResp struct {
	ID      string `json:"id"`
	Message string `json:"message"`
}

type doc struct {
	Entities []entity `json:"entities"`
	ID       string   `json:"id"`
}

type entity struct {
	Name              string    `json:"name"`
	Matches           []matches `json:"matches"`
	WikipediaLanguage string    `json:"wikipediaLanguage"`
	WikipediaId       string    `json:"wikipediaId"`
	WikipediaUrl      string    `json:"wikipediaUrl"`
	BingId            string    `json:"bingId"`
	Type              string    `json:"type"`
	SubType           string    `json:"subType"`
}

type matches struct {
	WikipediaScore  float64 `json:"wikipediaScore"`
	EntityTypeScore float64 `json:"entityTypeScore"`
	Text            string  `json:"text"`
	Offset          int     `json:"offset"`
	Length          int     `json:"length"`
}

// NewAnalyzer returns new Azure Analyzer
func NewAnalyzer(outputDir string) provider.Provider {
	return &Analyzer{outputDir: outputDir}
}

func (a *Analyzer) Name() string {
	return "Azure Text Analytics"
}

func (a *Analyzer) ShortName() string {
	return "azure"
}

func (a *Analyzer) AnalyzeEntities(input []byte, lang string) ([]byte, float64, error) {
	subscriptionKey := os.Getenv("TEXT_ANALYTICS_SUBSCRIPTION_KEY")
	if len(subscriptionKey) == 0 {
		return nil, 0.0, fmt.Errorf("please set/export the environment variable TEXT_ANALYTICS_SUBSCRIPTION_KEY")
	}

	endpoint := os.Getenv("TEXT_ANALYTICS_ENDPOINT")
	if len(endpoint) == 0 {
		return nil, 0.0, fmt.Errorf("please set/export the environment variable TEXT_ANALYTICS_ENDPOINT")
	}

	r := bytes.NewReader(input)

	req, err := http.NewRequest(http.MethodPost, endpoint+uriPath, r)
	if err != nil {
		return nil, 0.0, err
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Ocp-Apim-Subscription-Key", subscriptionKey)

	start := time.Now()
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Printf("[%s] analyzing error: %s\n", a.Name(), err)
		return nil, 0.0, err
	}
	elapsed := time.Since(start).Seconds()
	defer resp.Body.Close()

	output, err := ioutil.ReadAll(resp.Body)
	return output, elapsed, err
}

func (a *Analyzer) SaveOutput(file string, output []byte) error {
	return a.saveToFile(file, output)
}

func (a *Analyzer) GetEntities(data []byte) ([]string, error) {
	return nil, nil
}

func (a *Analyzer) SaveMatch(match float64, file string) error {
	return a.saveToFile(file, []byte(fmt.Sprintf("%.2f", match)))
}

func (a *Analyzer) saveToFile(file string, data []byte) error {
	if _, err := os.Stat(a.outputDir + "azure"); os.IsNotExist(err) {
		err = os.MkdirAll(a.outputDir+"azure", 0700)
	}

	f, err := os.Create(a.outputDir + "azure/" + file)
	if err != nil {
		return err
	}

	defer f.Close()
	_, err = f.Write(data)
	return err
}

package textrazor

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"

	"bitbucket.org/repustate/named-entity-recognition-apis/pkg/provider"
	l "bitbucket.org/repustate/named-entity-recognition-apis/pkg/utils/language"
)

const (
	api = "https://api.textrazor.com/"
)

// Analyzer represents TextRazor client
type Analyzer struct {
	outputDir string
}

type output struct {
	Response struct {
		Entities []struct {
			MatchedText string `json:"matchedText"`
		} `json:"entities"`
	} `json:"response"`
}

// NewAnalyzer returns new TextRazor Analyzer
func NewAnalyzer(outputDir string) provider.Provider {
	return &Analyzer{outputDir: outputDir}
}

func (a *Analyzer) Name() string {
	return "TextRazor"
}

func (a *Analyzer) ShortName() string {
	return "textrazor"
}

func (a *Analyzer) AnalyzeEntities(input []byte, _ string) ([]byte, float64, error) {
	data := url.Values{
		"extractors": []string{"entities"},
		"text":       []string{string(input)},
	}

	req, err := http.NewRequest(http.MethodPost, api, strings.NewReader(data.Encode()))
	if err != nil {
		return nil, 0.0, err
	}

	req.Header.Add("x-textrazor-key", os.Getenv("TEXTRAZOR_API_KEY"))
	start := time.Now()
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Printf("[%s] analyzing error: %s\n", a.Name(), err)
		return l.ServerError(), 0.0, nil
	}
	defer resp.Body.Close()
	elapsed := time.Since(start).Seconds()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, 0.0, err
	}

	var out bytes.Buffer
	err = json.Indent(&out, b, "", "  ")

	return out.Bytes(), elapsed, err
}

func (a *Analyzer) SaveOutput(file string, output []byte) error {
	return a.saveToFile(file, output)
}

func (a *Analyzer) GetEntities(data []byte) ([]string, error) {
	var o output
	err := json.Unmarshal(data, &o)
	if err != nil {
		return nil, err
	}

	entities := make([]string, 0, len(o.Response.Entities))

	for _, e := range o.Response.Entities {
		entities = append(entities, e.MatchedText)
	}
	return entities, nil
}

func (a *Analyzer) SaveMatch(match float64, file string) error {
	return a.saveToFile(file, []byte(fmt.Sprintf("%.2f", match)))
}

func (a *Analyzer) saveToFile(file string, data []byte) error {
	if _, err := os.Stat(a.outputDir + "textrazor"); os.IsNotExist(err) {
		err = os.MkdirAll(a.outputDir+"textrazor", 0700)
	}

	f, err := os.Create(a.outputDir + "textrazor/" + file)
	if err != nil {
		return err
	}

	defer f.Close()
	_, err = f.Write(data)
	return err
}

package gcp

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"os"
	"time"

	"bitbucket.org/repustate/named-entity-recognition-apis/pkg/provider"
	l "bitbucket.org/repustate/named-entity-recognition-apis/pkg/utils/language"

	language "cloud.google.com/go/language/apiv1"
	languagepb "google.golang.org/genproto/googleapis/cloud/language/v1"
)

var (
	supportedLanguages = map[string]bool{
		"zh":      true,
		"zh-Hant": true,
		"en":      true,
		"fr":      true,
		"de":      true,
		"it":      true,
		"ja":      true,
		"ko":      true,
		"pt":      true,
		"ru":      true,
		"es":      true,
	}
)

// Analyzer represents GCP Natural Language client
type Analyzer struct {
	outputDir string
}

type output struct {
	Entities []struct {
		Name string `json:"name"`
	} `json:"entities"`
}

// NewAnalyzer returns new GCP Analyzer
func NewAnalyzer(outputDir string) provider.Provider {
	return &Analyzer{outputDir: outputDir}
}

func (a *Analyzer) Name() string {
	return "GCP Natural Language"
}

func (a *Analyzer) ShortName() string {
	return "gcp"
}

func (a *Analyzer) AnalyzeEntities(input []byte, lang string) ([]byte, float64, error) {
	if _, ok := supportedLanguages[lang]; !ok {
		return l.UnsupportedResponse(), 0.0, nil
	}
	ctx := context.Background()
	client, err := language.NewClient(ctx)
	if err != nil {
		return nil, 0.0, err
	}

	start := time.Now()
	r, err := client.AnalyzeEntities(ctx, &languagepb.AnalyzeEntitiesRequest{
		Document: &languagepb.Document{
			Source: &languagepb.Document_Content{
				Content: string(input),
			},
			Type:     languagepb.Document_PLAIN_TEXT,
			Language: lang,
		},
		EncodingType: languagepb.EncodingType_UTF8,
	})
	if err != nil {
		fmt.Printf("[%s] analyzing error: %s\n", a.Name(), err)
		return l.UnsupportedResponse(), 0.0, nil
	}
	elapsed := time.Since(start).Seconds()

	b, err := json.Marshal(r)
	if err != nil {
		return nil, 0.0, err
	}

	var out bytes.Buffer
	err = json.Indent(&out, b, "", "  ")

	return out.Bytes(), elapsed, err
}

func (a *Analyzer) SaveOutput(file string, output []byte) error {
	return a.saveToFile(file, output)
}

func (a *Analyzer) GetEntities(data []byte) ([]string, error) {
	var o output
	err := json.Unmarshal(data, &o)
	if err != nil {
		return nil, err
	}

	entities := make([]string, 0, len(o.Entities))

	for _, e := range o.Entities {
		entities = append(entities, e.Name)
	}
	return entities, nil
}

func (a *Analyzer) SaveMatch(match float64, file string) error {
	return a.saveToFile(file, []byte(fmt.Sprintf("%.2f", match)))
}

func (a *Analyzer) saveToFile(file string, data []byte) error {
	if _, err := os.Stat(a.outputDir + "gcp"); os.IsNotExist(err) {
		err = os.MkdirAll(a.outputDir+"gcp", 0700)
	}

	f, err := os.Create(a.outputDir + "gcp/" + file)
	if err != nil {
		return err
	}

	defer f.Close()
	_, err = f.Write(data)
	return err
}

package aws

import (
	"bytes"
	"encoding/json"
	"fmt"
	"os"
	"time"

	"bitbucket.org/repustate/named-entity-recognition-apis/pkg/provider"
	l "bitbucket.org/repustate/named-entity-recognition-apis/pkg/utils/language"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/comprehend"
)

var (
	supportedLanguages = map[string]bool{
		"en": true,
		"es": true,
		"fr": true,
		"de": true,
		"it": true,
		"pt": true,
	}
)

// Analyzer represents AWS Comprehend client
type Analyzer struct {
	outputDir string
}

type output struct {
	Entities []struct {
		Text string `json:"Text"`
	} `json:"Entities"`
}

// NewAnalyzer returns new AWS Analyzer
func NewAnalyzer(outputDir string) provider.Provider {
	return &Analyzer{outputDir: outputDir}
}

func (a *Analyzer) Name() string {
	return "AWS Comprehend"
}

func (a *Analyzer) ShortName() string {
	return "aws"
}

func (a *Analyzer) AnalyzeEntities(input []byte, lang string) ([]byte, float64, error) {
	if _, ok := supportedLanguages[lang]; !ok {
		return l.UnsupportedResponse(), 0.0, nil
	}
	sess, err := session.NewSession(&aws.Config{
		Region:      aws.String(os.Getenv("AWS_REGION")),
		Credentials: credentials.NewSharedCredentials("", os.Getenv("AWS_PROFILE")),
	})
	if err != nil {
		return nil, 0.0, err
	}
	svc := comprehend.New(sess)
	start := time.Now()
	output, err := svc.DetectEntities(&comprehend.DetectEntitiesInput{
		LanguageCode: aws.String(lang),
		Text:         aws.String(string(input)),
	})
	if err != nil {
		fmt.Printf("[%s] analyzing error: %s\n", a.Name(), err)
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case comprehend.ErrCodeTextSizeLimitExceededException:
				return l.SizeLimitExceededResponse(), 0.0, nil
			default:
				return l.UnsupportedResponse(), 0.0, nil
			}
		}
	}
	elapsed := time.Since(start).Seconds()
	b, err := json.Marshal(output)
	if err != nil {
		return nil, 0.0, err
	}

	var out bytes.Buffer
	err = json.Indent(&out, b, "", "  ")

	return out.Bytes(), elapsed, err
}

func (a *Analyzer) SaveOutput(file string, output []byte) error {
	return a.saveToFile(file, output)
}

func (a *Analyzer) GetEntities(data []byte) ([]string, error) {
	var o output
	err := json.Unmarshal(data, &o)
	if err != nil {
		return nil, err
	}

	entities := make([]string, 0, len(o.Entities))

	for _, e := range o.Entities {
		entities = append(entities, e.Text)
	}
	return entities, nil
}

func (a *Analyzer) SaveMatch(match float64, file string) error {
	return a.saveToFile(file, []byte(fmt.Sprintf("%.2f", match)))
}

func (a *Analyzer) saveToFile(file string, data []byte) error {
	if _, err := os.Stat(a.outputDir + "aws"); os.IsNotExist(err) {
		err = os.MkdirAll(a.outputDir+"aws", 0700)
	}

	f, err := os.Create(a.outputDir + "aws/" + file)
	if err != nil {
		return err
	}

	defer f.Close()
	_, err = f.Write(data)
	return err
}

package provider

// Provider represents general interface for interacting with Natural Language APIs
type Provider interface {
	Name() string
	ShortName() string
	AnalyzeEntities(input []byte, lang string) ([]byte, float64, error)
	SaveOutput(file string, output []byte) error
	GetEntities(output []byte) ([]string, error)
	SaveMatch(match float64, file string) error
}

package dandelion

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"

	"bitbucket.org/repustate/named-entity-recognition-apis/pkg/provider"
	l "bitbucket.org/repustate/named-entity-recognition-apis/pkg/utils/language"
)

const (
	api = "https://api.dandelion.eu/datatxt/nex/v1"
)

var (
	supportedLanguages = map[string]bool{
		"de": true,
		"en": true,
		"es": true,
		"fr": true,
		"it": true,
		"pt": true,
		"ru": true,
		"af": true,
		"sq": true,
		"ar": true,
		"bn": true,
		"bg": true,
		"hr": true,
		"cs": true,
		"da": true,
		"nl": true,
		"et": true,
		"fi": true,
		"el": true,
		"gu": true,
		"he": true,
		"hi": true,
		"hu": true,
		"id": true,
		"ja": true,
		"kn": true,
		"ko": true,
		"lv": true,
		"lt": true,
		"mk": true,
		"ml": true,
		"mr": true,
		"ne": true,
		"no": true,
		"pa": true,
		"fa": true,
		"pl": true,
		"ro": true,
		"sk": true,
		"sl": true,
		"sw": true,
		"sv": true,
		"tl": true,
		"ta": true,
		"te": true,
		"th": true,
		"tr": true,
		"uk": true,
		"ur": true,
		"vi": true,
	}
)

// Analyzer represents Dandelion client
type Analyzer struct {
	outputDir string
}

type output struct {
	Annotations []struct {
		Spot string `json:"spot"`
	} `json:"annotations"`
}

// NewAnalyzer returns new Dandelion Analyzer
func NewAnalyzer(outputDir string) provider.Provider {
	return &Analyzer{outputDir: outputDir}
}

func (a *Analyzer) Name() string {
	return "Dandelion"
}

func (a *Analyzer) ShortName() string {
	return "dandelion"
}

func (a *Analyzer) AnalyzeEntities(input []byte, lang string) ([]byte, float64, error) {
	if _, ok := supportedLanguages[lang]; !ok {
		lang = "auto"
	}

	req, err := http.NewRequest(http.MethodGet, api, nil)
	if err != nil {
		return nil, 0.0, err
	}

	q := req.URL.Query()
	q.Add("token", os.Getenv("DANDELION_API_TOKEN"))
	q.Add("lang", lang)
	q.Add("text", string(input))
	req.URL.RawQuery = q.Encode()
	start := time.Now()
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Printf("[%s] analyzing error: %s\n", a.Name(), err)
		return l.ServerError(), 0.0, nil
	}
	elapsed := time.Since(start).Seconds()
	defer resp.Body.Close()

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, 0.0, err
	}

	var out bytes.Buffer
	err = json.Indent(&out, b, "", "  ")

	return out.Bytes(), elapsed, err
}

func (a *Analyzer) SaveOutput(file string, output []byte) error {
	return a.saveToFile(file, output)
}

func (a *Analyzer) GetEntities(data []byte) ([]string, error) {
	var o output
	err := json.Unmarshal(data, &o)
	if err != nil {
		return nil, err
	}

	entities := make([]string, 0, len(o.Annotations))

	for _, e := range o.Annotations {
		entities = append(entities, e.Spot)
	}
	return entities, nil
}

func (a *Analyzer) SaveMatch(match float64, file string) error {
	return a.saveToFile(file, []byte(fmt.Sprintf("%.2f", match)))
}

func (a *Analyzer) saveToFile(file string, data []byte) error {
	if _, err := os.Stat(a.outputDir + "dandelion"); os.IsNotExist(err) {
		err = os.MkdirAll(a.outputDir+"dandelion", 0700)
	}

	f, err := os.Create(a.outputDir + "dandelion/" + file)
	if err != nil {
		return err
	}

	defer f.Close()
	_, err = f.Write(data)
	return err
}

package aylien

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"

	"bitbucket.org/repustate/named-entity-recognition-apis/pkg/provider"
	l "bitbucket.org/repustate/named-entity-recognition-apis/pkg/utils/language"
)

const (
	api   = "https://api.aylien.com/api/v1/entities/"
	delay = 1500 * time.Millisecond // 1.5 second
)

var supportedLanguages = map[string]bool{
	"en": true,
	"de": true,
	"fr": true,
	"it": true,
	"es": true,
	"pt": true,
}

// Analyzer represents Aylien client
type Analyzer struct {
	outputDir string

	delayer <-chan time.Time
}

type output struct {
	Entities struct {
		Keyword []string `json:"keyword"`
	} `json:"entities"`
}

// NewAnalyzer returns new Aylien Analyzer
func NewAnalyzer(outputDir string) provider.Provider {
	return &Analyzer{
		outputDir: outputDir,
		delayer:   time.Tick(delay),
	}
}

func (a *Analyzer) Name() string {
	return "Aylien"
}

func (a *Analyzer) ShortName() string {
	return "aylien"
}

func (a *Analyzer) AnalyzeEntities(input []byte, lang string) ([]byte, float64, error) {
	// Aylien API allows 60 requests per 60 seconds
	// So there is a delayer to make no more than 1 request per 1.5 seconds
	<-a.delayer

	if _, ok := supportedLanguages[lang]; !ok {
		return l.UnsupportedResponse(), 0.0, nil
	}
	data := url.Values{
		"language": []string{lang},
		"text":     []string{string(input)},
	}

	req, err := http.NewRequest(http.MethodPost, api, strings.NewReader(data.Encode()))
	if err != nil {
		return nil, 0.0, err
	}

	req.Header.Add("x-aylien-textapi-application-key", os.Getenv("AYLIEN_API_KEY"))
	req.Header.Add("x-aylien-textapi-application-id", os.Getenv("AYLIEN_APP_ID"))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	start := time.Now()
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Printf("[%s] analyzing error: %s\n", a.Name(), err)
		return l.ServerError(), 0.0, nil
	}
	elapsed := time.Since(start).Seconds()
	defer resp.Body.Close()

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, 0.0, err
	}

	var out bytes.Buffer
	err = json.Indent(&out, b, "", "  ")

	return out.Bytes(), elapsed, err
}

func (a *Analyzer) SaveOutput(file string, output []byte) error {
	return a.saveToFile(file, output)
}

func (a *Analyzer) GetEntities(data []byte) ([]string, error) {
	var o output
	err := json.Unmarshal(data, &o)
	if err != nil {
		return nil, err
	}

	entities := make([]string, 0, len(o.Entities.Keyword))

	for _, e := range o.Entities.Keyword {
		entities = append(entities, e)
	}
	return entities, nil
}

func (a *Analyzer) SaveMatch(match float64, file string) error {
	return a.saveToFile(file, []byte(fmt.Sprintf("%.2f", match)))
}

func (a *Analyzer) saveToFile(file string, data []byte) error {
	if _, err := os.Stat(a.outputDir + "aylien"); os.IsNotExist(err) {
		err = os.MkdirAll(a.outputDir+"aylien", 0700)
	}

	f, err := os.Create(a.outputDir + "aylien/" + file)
	if err != nil {
		return err
	}

	defer f.Close()
	_, err = f.Write(data)
	return err
}

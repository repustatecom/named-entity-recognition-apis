This resonates with me. I've spent 1 year+ in each of C, C++, C#,
	PHP, Java, Python, and JS. There are obvious benefits to taking advantage
	of a platform's strengths, but the "plain" code has merits too. Easier to
	reason about, often more maintainable, faster to ramp up new team members,
	etc.
It being incorectly tagged The Slackware Linux distribution (v.
	1.00) is now available for anonymous FTP. This is a complete installation
	system designed for systems with a 3.5" boot floppy. It has been tested
	extensively with a 386/IDE system. The standard kernel included does not
	support SCSI, but if there's a great demand, I might be persuaded to
	compile a few custom kernels to put up for FTP.
Such principles led the advisor to buy 786,700 shares of Gilead in
	the first quarter. After rallying in January before fading, the biotech’s
	stock managed a 6% gain in the first three months of 2018, easily
	outstripping the S&P 500 which slipped 1.2%. Gilead’s fourth-quarter
	report in February beat expectations in terms of revenue and earnings per
	share, but revenue from hepatitis C virus (HCV) products tumbled to $1.5
	billion in the quarter sequentially from $2.2 billion in the third
	quarter.
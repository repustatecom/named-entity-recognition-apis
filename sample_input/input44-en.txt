He contrasts this with "old tech" like Microsoft (MSFT), Oracle
	(ORCL), and SAP (SAP), which has seen strong relative performance that's
	been driven more by multiple expansion, as the group's multiple has
	expanded by 2 times compared to the S&P 500 since the end of last year.
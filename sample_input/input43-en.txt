Mega-cap tech stocks like Apple ($AAPL), Google ($GOOG), Amazon
	($AMZN), and Facebook ($FB) have had a good year for sure, and Apple and
	Facebook are both up double-digits this summer, while Amazon has about
	paced the market and Google has trailed a bit.
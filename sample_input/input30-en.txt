If you were drawing up the kind of player the Raptors
	would benefit the most from in this buyout season for NBA veterans, Jeremy
	Lin would fit the bill perfectly. He’s a veteran backcourt player who is a
	solid three-point shooter, an effective pick-and-roll player with no sense
	of entitlement for shots and playing time and role.
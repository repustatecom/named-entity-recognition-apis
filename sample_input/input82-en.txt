Roe v. Wade, which was decided by the Supreme Court on January 22,
	1973, affirms the constitutional right to access safe, legal abortion. More
	than 40 years later, Americans overwhelmingly support the decision.
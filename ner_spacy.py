from ast import literal_eval
import json
import os
import re
import spacy
import time

# dict of spacy trained language models
languageModels = {
    "de": "de_core_news_sm",
    "en": "en_core_web_sm",
    "es": "es_core_news_sm",
    "fr": "fr_core_news_sm",
    "it": "it_core_news_sm",
    "nl": "nl_core_news_sm",
    "pt": "pt_core_news_sm",
}

inputBasepath = 'sample_input/'
outputBasepath = 'sample_output/spacy/'
expectedBasepath = 'sample_expected/'
resultsBasepath = 'results/spacy/'

def main():
    matchings = []
    execTimes = []
    for entry in os.listdir(inputBasepath):
        f = open(os.path.join(inputBasepath, entry), "r")
        if f.mode == 'r':
            print("[Spacy] Analyzing file: %s" % entry)
            # get input number and language from the file name
            search = re.search('input(\d+)\-(.*)\.txt', entry, re.IGNORECASE)
            inputNum = search.group(1)
            lang = search.group(2)

            # try to load language specific model, otherwise use multi-language model
            nlp = spacy.load(languageModels.get(lang, "xx_ent_wiki_sm"))
            start_time = time.time()
            doc = nlp(f.read())
            elapsed = (time.time() - start_time)
            execTimes.append(elapsed)
            # create output dir if it does not exist
            if not os.path.exists(outputBasepath):
                os.makedirs(outputBasepath)

            # write the results to the output json file
            with open(os.path.join(outputBasepath, "output-%s-%s.json" % (inputNum, lang)), "w") as outfile:
                json.dump(dict([(str(x), x.label_) for x in doc.ents]), outfile, ensure_ascii=False, indent=4)


            # calculate matching
            matchingFile = open(os.path.join(outputBasepath, "matching-%s-%s.txt" % (inputNum, lang)), "w")
            expFile = open(os.path.join(expectedBasepath, "expected%s-%s.txt" % (inputNum, lang)), "r")
            expected = literal_eval(expFile.read())
            current = list(map(str, doc.ents))
            if len(expected) == len(set(expected) & set(current)):
                match = "1.0"
            else:
                try:
                    match = "{:.2f}".format(len(set(expected) & set(current)) / float(len(set(expected))))
                except ZeroDivisionError:
                    match = "0.0"
            matchings.append(float(match))
            matchingFile.write(match)
            matchingFile.close()

    # create results dir if it does not exist
    if not os.path.exists(resultsBasepath):
        os.makedirs(resultsBasepath)

    # calculate and save average matching result
    avgMatchingFile = open(os.path.join(resultsBasepath, "average_match.txt"), "w")
    avgMatchingFile.write( "{:.2f}".format(sum(matchings) / float(len(matchings))))
    avgMatchingFile.close()

    # calculate and save average execution time
    avgRespTimeFile = open(os.path.join(resultsBasepath, "average_response_time.txt"), "w")
    avgRespTimeFile.write( "{:.2f}".format(sum(execTimes) / float(len(execTimes))))
    avgRespTimeFile.close()

if __name__== "__main__":
    main()
